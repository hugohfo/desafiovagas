var app = angular.module('app',['ngRoute']);
 
app.config(function($routeProvider, $locationProvider)
{
	// remove o # da url
	$locationProvider.html5Mode(true);
 
	$routeProvider
     
       // para a rota '/', carregaremos o template home.html e o controller 'HomeCtrl'
       .when('/', {
          templateUrl : 'app/views/home.php',
          controller     : 'HomeCtrl',
       })
     
       // para a rota '/sobre', carregaremos o template sobre.html e o controller 'SobreCtrl'
       .when('/videos', {
          templateUrl : 'app/views/videos.php',
          controller  : 'VideoCtrl',
       })
     
       // para a rota '/sobre', carregaremos o template sobre.html e o controller 'SobreCtrl'
       .when('/teste', {
          templateUrl : 'app/views/teste.php',
          controller  : 'TesteCtrl',
       })
     
       // caso não seja nenhum desses, redirecione para a rota '/'
       .otherwise ({ redirectTo: '/' });
	   
});