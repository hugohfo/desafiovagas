<nav class="navbar bg-faded default-menu">
  <div class="layout_width" style="display: block; margin: auto;">

    <a class="navbar-brand" href="#"><i class="fa fa-youtube-play fa-lg person"></i>Fatos Desconhecidos</a>
    

    <ul class="nav navbar-nav pull-xs-right">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars fa-lg person"></i></a>

        <div class="dropdown-menu">
          <a class="dropdown-item" ng-class="{active: activetab == '/'}" href="/desafiovagas/" onclick="handleAPILoaded()">Destaques</a>
          <a class="dropdown-item" id="pagVideos" ng-class="{active: activetab == '/desafiovagas/videos'}" href="/desafiovagas/videos" onclick="handleAPILoaded()">Vídeos</a>
        </div>

      </li>
    </ul>

    <form class="form-inline pull-xs-right">
      <input id="query" class="form-control" value="" type="text" placeholder="Procurar">
      <button class="btn btn-success-outline" id="search-button" disabled onclick="search()"><i class="fa fa-search fa-lg"></i></button>
    </form>
  </div>
</nav>

