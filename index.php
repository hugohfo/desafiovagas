<!DOCTYPE html >
<html ng-app="app">
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
	
	<base href="/desafiovagas/">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="/desafiovagas/public/js/bootstrap.min.js"></script>

    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.8/angular.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.0rc1/angular-route.min.js"></script>

    <script src="/desafiovagas/app/lib/routes.js"></script>
    <script src="/desafiovagas/app/controllers/controllers.js"></script>

    <script src="/desafiovagas/public/js/auth.js"></script>
    <script src="/desafiovagas/public/js/search.js"></script>
    <script src="https://apis.google.com/js/client.js?onload=googleApiClientReady"></script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/desafiovagas/public/css/app.min.css">

  </head>
<body>


<?php include_once"app/partials/menu.php"; ?>

 
      <div ng-view></div>
 

   </body>

</html>